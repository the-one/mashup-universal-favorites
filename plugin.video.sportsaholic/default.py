#-*- coding: utf-8 -*-
import urllib,urllib2,re,cookielib,string, urlparse
import xbmc, xbmcgui, xbmcaddon, xbmcplugin
import os
from t0mm0.common.addon import Addon
from t0mm0.common.net import Net as net
import datetime,time
from resources.libs import main


#Sports-A-Holic - by Mash2k3 2013.

addon_id = 'plugin.video.sportsaholic'
selfAddon = xbmcaddon.Addon(id=addon_id)
addon = Addon(addon_id)
art = xbmc.translatePath(os.path.join('special://home/addons/plugin.video.sportsaholic/art', ''))
try:
    import watchhistory
except:
    import watchhistorydummy as watchhistory
wh = watchhistory.WatchHistory('plugin.video.sportsaholic')


################################################################################ Source Imports ##########################################################################################################

from resources.libs import youtube
from resources.libs.live import  vipplaylist
from resources.libs.sports import wildtv, golfchannel,  fitnessblender, skysports, tsn, espn, foxsoccer, outdoorch, mmafighting, bodybuilding

################################################################################ Directories ##########################################################################################################


def MAIN():
        main.addDir("My Favorites",'Favorites',650,art+'/fav.png')
        main.addDir('Watch History','history',222,art+'/whistory.png')
        main.addDir('ESPN','http:/espn.com',44,art+'/espn.png')
        main.addDir('TSN','http:/tsn.com',95,art+'/tsn.png')
        main.addDir('SkySports.com','www1.skysports.com',172,art+'/skysports.png')
        main.addDir('Fox Soccer  [COLOR red](US ONLY)[/COLOR]','http:/tsn.com',124,art+'/foxsoc.png')
        main.addDir('All MMA','mma',537,art+'/mma.png')
        main.addDir('Outdoor Channel','http://outdoorchannel.com/',50,art+'/OC.png')
        main.addDir('Wild TV','https://www.wildtv.ca/shows',92,art+'/wildtv.png')
        main.addDir('Workouts','https://www.wildtv.ca/shows',194,art+'/workout.png')
        main.addDir('The Golf Channel','golf',217,art+'/golfchannel.png')
        main.addDir('K1m05 Sports','http://mash2k3-repository.googlecode.com/svn/trunk/k1m05%27s%20playlist/sportsk1.xml',182,art+'/k1m05.png')
        main.addDir('Max Powers Sports','http://mash2k3-repository.googlecode.com/svn/trunk/maxpowers%20playlist/sportsmax',182,art+'/maxpowers.png')
        main.addDir('Crusader Sports','https://github.com/mash2k3/MashUpStreams/raw/master/CrusaderStreams/sports.xml',182,art+'/crusader.png')
        main.addPlayc('Need Help?','http://www.movie25.com/',100,art+'/xbmchub.png','','','','','')
        main.VIEWSB()


def MMA():
        main.addDir('UFC','ufc',47,art+'/ufc.png')
        main.addDir('Bellator','BellatorMMA',47,art+'/bellator.png')
        main.addDir('MMA Fighting.com','http://www.mmafighting.com/videos',113,art+'/mmafig.png')
        main.GA("None","MMA")

def WorkoutMenu():
        main.addDir('Fitness Blender[COLOR red](Full Workouts)[/COLOR]','fb',198,art+'/fitnessblender.png')
        main.addDir('Body Building[COLOR red](Instructional Only)[/COLOR]','bb',195,art+'/bodybuilding.png')
        main.GA("None","Workout")





################################################################################ XBMCHUB POPUP ##########################################################################################################


class HUB( xbmcgui.WindowXMLDialog ):
    def __init__( self, *args, **kwargs ):
        self.shut = kwargs['close_time'] 
        xbmc.executebuiltin( "Skin.Reset(AnimeWindowXMLDialogClose)" )
        xbmc.executebuiltin( "Skin.SetBool(AnimeWindowXMLDialogClose)" )
                                       
    def onInit( self ):
        xbmc.Player().play('%s/resources/skins/DefaultSkin/media/xbmchub.mp3'%selfAddon.getAddonInfo('path'))# Music.
        while self.shut > 0:
            xbmc.sleep(1000)
            self.shut -= 1
        xbmc.Player().stop()
        self._close_dialog()
                
    def onFocus( self, controlID ): pass
    
    def onClick( self, controlID ): 
        if controlID == 12:
            xbmc.Player().stop()
            self._close_dialog()
        if controlID == 7:
            xbmc.Player().stop()
            self._close_dialog()

    def onAction( self, action ):
        if action in [ 5, 6, 7, 9, 10, 92, 117 ] or action.getButtonCode() in [ 275, 257, 261 ]:
            xbmc.Player().stop()
            self._close_dialog()

    def _close_dialog( self ):
        xbmc.executebuiltin( "Skin.Reset(AnimeWindowXMLDialogClose)" )
        time.sleep( .4 )
        self.close()
        
def pop():
    if xbmc.getCondVisibility('system.platform.ios'):
        if not xbmc.getCondVisibility('system.platform.atv'):
            popup = HUB('hub1.xml',selfAddon.getAddonInfo('path'),'DefaultSkin',close_time=34,logo_path='%s/resources/skins/DefaultSkin/media/Logo/'%selfAddon.getAddonInfo('path'))
    if xbmc.getCondVisibility('system.platform.android'):
        popup = HUB('hub1.xml',selfAddon.getAddonInfo('path'),'DefaultSkin',close_time=34,logo_path='%s/resources/skins/DefaultSkin/media/Logo/'%selfAddon.getAddonInfo('path'))
    else:
        popup = HUB('hub.xml',selfAddon.getAddonInfo('path'),'DefaultSkin',close_time=34,logo_path='%s/resources/skins/DefaultSkin/media/Logo/'%selfAddon.getAddonInfo('path'))
    popup.doModal()
    del popup

################################################################################ Favorites Function##############################################################################################################




def ListglobalFavMs():
        favpath=os.path.join(main.datapath,'Favourites')
        tvfav=os.path.join(favpath,'Misc')
        FavFile=os.path.join(tvfav,'MiscFav')
        if os.path.exists(FavFile):
                Favs=re.compile('url="(.+?)",name="(.+?)",mode="(.+?)",thumb="(.+?)",plot="(.+?)",type="(.+?)"').findall(open(FavFile,'r').read())
                for url,name,mode,thumb,plot,type in Favs:
                        if type=='PLAY':
                                main.addPlayMs(name,url,int(mode),thumb,plot,'','','','')
                        if type=='DIR':
                                main.addDirMs(name,url,int(mode),thumb,plot,'','','','')
                
        else:
                xbmc.executebuiltin("XBMC.Notification([B][COLOR green]Sports-A-Holic[/COLOR][/B],[B]You Have No Saved Favourites[/B],5000,"")")
        main.GA("None","Sportsaholic-Fav")
        xbmcplugin.setContent(int(sys.argv[1]), 'Movies')


################################################################################ Histroy ##########################################################################################################

def History():
    main.GA("None","WatchHistory")
    if selfAddon.getSetting("whistory") == "true":
        history_items = wh.get_my_watch_history()
        for item in history_items:
                item_title = item['title']
                item_url = item['url']
                item_image = item['image_url']
                item_fanart = item['fanart_url']
                item_infolabels = item['infolabels']
                item_isfolder = item['isfolder']
                if item_image =='':
                    item_image= art+'/noimage.png'
                main.addLink(item_title,item_url,item_image)
    else:
        dialog = xbmcgui.Dialog()
        ok=dialog.ok('[B]MashUp History[/B]', 'Watch history is disabled' ,'To enable go to addon settings','and enable Watch History')
        history_items = wh.get_my_watch_history()
        for item in history_items:
                item_title = item['title']
                item_url = item['url']
                item_image = item['image_url']
                item_fanart = item['fanart_url']
                item_infolabels = item['infolabels']
                item_isfolder = item['isfolder']
                main.addLink(item_title,item_url,item_image)
        


################################################################################ Modes ##########################################################################################################


def get_params():
        param=[]
        paramstring=sys.argv[2]
        if len(paramstring)>=2:
                params=sys.argv[2]
                cleanedparams=params.replace('?','')
                if (params[len(params)-1]=='/'):
                        params=params[0:len(params)-2]
                pairsofparams=cleanedparams.split('&')
                param={}
                for i in range(len(pairsofparams)):
                        splitparams={}
                        splitparams=pairsofparams[i].split('=')
                        if (len(splitparams))==2:
                                param[splitparams[0]]=splitparams[1]
                                
        return param
              
params=get_params()

url=None
name=None
mode=None
iconimage=None
fanart=None
plot=None
genre=None




try:
        name=urllib.unquote_plus(params["name"])
except:
        pass

try:
        
        url=urllib.unquote_plus(params["url"])
        
except:
        pass

try:
        mode=int(params["mode"])
except:
        pass

try:
        iconimage=urllib.unquote_plus(params["iconimage"])
        iconimage = iconimage.replace(' ','%20')
except:
        pass
try:
        plot=urllib.unquote_plus(params["plot"])
except:
        pass
try:
        fanart=urllib.unquote_plus(params["fanart"])
        fanart = fanart.replace(' ','%20')
except:
        pass

try:
        genre=urllib.unquote_plus(params["genre"])
except:
        pass


print "Mode: "+str(mode)
print "URL: "+str(url)
print "Name: "+str(name)
print "Thumb: "+str(iconimage)

if mode==None or url==None or len(url)<1:
        print ""
        MAIN()
       
        
elif mode==43:
        print ""+url
        SPORTS()

elif mode==44:
        print ""+url
        espn.ESPN()
        
elif mode==45:
        print ""+url
        espn.ESPNList(url)

elif mode==46:
        print ""+url
        espn.ESPNLink(name,url,iconimage,plot)

elif mode==47:
        print ""+url
        youtube.YOUList(name,url)
        
elif mode==48:
        print ""+url
        youtube.YOULink(name,url,iconimage)

elif mode==50:
        print ""+url
        outdoorch.OC()
        
elif mode==51:
        print ""+url
        outdoorch.OCList(url)

elif mode==52:
        print ""+url
        outdoorch.OCLink(name,url,iconimage)
        
elif mode==59:
        print ""+url
        UFC()
        
elif mode==92:
        print ""+url
        wildtv.WILDTV(url)        

elif mode==93:
        print ""+url
        wildtv.LISTWT(url)
        
elif mode==94:
        print ""+url
        wildtv.LINKWT(name,url)

elif mode==95:
        print ""+url
        tsn.TSNDIR()

elif mode==96:
        print ""+url
        tsn.TSNDIRLIST(url)        

elif mode==97:
        print ""+url
        tsn.TSNLIST(url)
        
elif mode==98:
        print ""+url
        tsn.TSNLINK(name,url,iconimage)
        
elif mode==100:
        pop()

elif mode==113:
        mmafighting.MMAFList(url)

elif mode==114:        
        mmafighting.MMAFLink(name,url,iconimage)   


elif mode==124:
        foxsoccer.FOXSOC()
elif mode==125:
        foxsoccer.FOXSOCList(url)
elif mode==126:
        foxsoccer.FOXSOCLink(name,url)


elif mode==172:
        print ""+url
        skysports.SKYSPORTS()

elif mode==173:
        print ""+url
        skysports.SKYSPORTSList(url)

elif mode==174:
        print ""+url
        skysports.SKYSPORTSLink(name,url)

elif mode==175:
        print ""+url
        skysports.SKYSPORTSTV(url)

elif mode==176:
        print ""+url
        skysports.SKYSPORTSList2(url)
        
elif mode==177:
        dialog = xbmcgui.Dialog()
        dialog.ok("Mash Up", "Sorry this video requires a SkySports Suscription.","Will add this feature in later Version.","Enjoy the rest of the videos ;).")

elif mode==178:
        print ""+url
        skysports.SKYSPORTSCAT()

elif mode==179:
        print ""+url
        skysports.SKYSPORTSCAT2(url)

elif mode==180:
        print ""+url
        skysports.SKYSPORTSTEAMS(url)

elif mode==181:
        print ""+url
        vipplaylist.VIPplaylists(url)

elif mode==182:
        print ""+url
        vipplaylist.VIPList(name,url)

elif mode==183:
        print ""+url
        vipplaylist.VIPLink(name,url,iconimage)

elif mode==194:
        print ""+url
        WorkoutMenu()

elif mode==195:
        print ""+url
        bodybuilding.MAINBB()

elif mode==196:
        print ""+url
        bodybuilding.LISTBB(url)

elif mode==197:
        print ""+url
        bodybuilding.LINKBB(name,url,iconimage)

elif mode==198:
        print ""+url
        fitnessblender.MAINFB()

elif mode==199:
        print ""+url
        fitnessblender.BODYFB()

elif mode==200:
        print ""+url
        fitnessblender.DIFFFB()

elif mode==201:
        print ""+url
        fitnessblender.TRAINFB()

elif mode==202:
        print ""+url
        fitnessblender.LISTBF(url)

elif mode==203:
        print ""+url
        fitnessblender.LINKBB(name,url,iconimage)

elif mode==217:
        golfchannel.MAIN()
        
elif mode==218:
        print ""+url
        golfchannel.LIST(url)

elif mode==219:
        print ""+url
        golfchannel.LIST2(name,url,iconimage,plot)


elif mode==220:
        print ""+url
        golfchannel.LINK(name,url,iconimage)

elif mode==221:
        print ""+url
        golfchannel.LIST3(url)


elif mode==222:
        print ""+url
        History()

        
elif mode==537:
        print ""+url
        MMA() 



elif mode==650:
        print ""+url
        ListglobalFavMs()
                                       
xbmcplugin.endOfDirectory(int(sys.argv[1]))
