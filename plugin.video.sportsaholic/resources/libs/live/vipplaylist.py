import urllib,urllib2,re,cookielib,sys,os
import xbmc, xbmcgui, xbmcaddon, xbmcplugin
from resources.libs import main

#Sports-a-Holic - by Mash2k3 2013.

from t0mm0.common.addon import Addon
addon_id = 'plugin.video.sportsaholic'
selfAddon = xbmcaddon.Addon(id=addon_id)
addon = Addon('plugin.video.sportsaholic', sys.argv)
art = xbmc.translatePath(os.path.join('special://home/addons/plugin.video.sportsaholic/art', ''))

try:
    import watchhistory
except:
    import watchhistorydummy as watchhistory
    
wh = watchhistory.WatchHistory('plugin.video.sportsaholic')



def VIPplaylists(murl):
        vip='unknown'
        poster=re.compile('k1m05').findall(murl)
        if len(poster)>0:
                vip='k1m05'
        poster2=re.compile('maxpowers').findall(murl)
        if len(poster2)>0:
                vip='MaxPowers'
        poster3=re.compile('Crusader').findall(murl)
        if len(poster3)>0:
                vip='Crusader'
        link=main.OPENURL(murl)
        link=link.replace('\r','').replace('\n','').replace('\t','').replace('&nbsp;','')
        match=re.compile('<name>(.+?)</name><link>(.+?)</link><thumbnail>(.+?)</thumbnail><date>(.+?)</date>').findall(link)
        for name,url,thumb,date in match:
            main.addDir(name+' [COLOR red] Updated '+date+'[/COLOR]',url,182,thumb)
        main.GA("Live",vip+"-Playlists")


def VIPList(mname,murl):
        vip='unknown'
        poster=re.compile('k1m05').findall(murl)
        if len(poster)>0:
                vip='k1m05'
        poster2=re.compile('maxpowers').findall(murl)
        if len(poster2)>0:
                vip='Maxpowers'
        poster3=re.compile('Crusader').findall(murl)
        if len(poster3)>0:
                vip='Crusader'
        mname  = mname.split('[C')[0]
        main.GA(vip+"-Playlists",mname)
        link=main.OPENURL(murl)
        link=link.replace('\r','').replace('\n','').replace('\t','').replace('&nbsp;','')
        match=re.compile('<title>([^<]+)</title.+?link>([^<]+)</link.+?thumbnail>([^<]+)</thumbnail>').findall(link)
        for name,url,thumb in sorted(match):
            main.addPlayMs(name+' [COLOR blue]'+vip+'[/COLOR]',url,183,thumb,'','','','','')


def VIPLink(mname,murl,thumb):
        main.GA(mname,"Watched")
        ok=True
        playlist = xbmc.PlayList(xbmc.PLAYLIST_VIDEO)
        playlist.clear()
        stream_url = murl
        listitem = xbmcgui.ListItem(thumbnailImage=thumb)
        listitem.setInfo('video', {'Title': mname, 'Genre': 'Live'} )
        
        playlist.add(stream_url,listitem)
        xbmcPlayer = xbmc.Player()
        xbmcPlayer.play(playlist)
        #WatchHistory
        if selfAddon.getSetting("whistory") == "true":
            wh.add_item(mname+' '+'[COLOR green]Live[/COLOR]', sys.argv[0]+sys.argv[2], infolabels='', img=thumb, fanart='', is_folder=False)
        return ok
